import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodoCreateRequestTests: XCTestCase {
    let body = TodoRequestBody(title: "テスト4", detail: nil, date: nil)

    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }

    func testInit() {
        let request = TodoCreateRequest(body: body)

        XCTAssertEqual(request.method, .post)
        XCTAssertEqual(request.path, "todos")
    }

    func testResponse() {
        var todoCreateResponse: CommonResponse?

        stub(condition: isHost("todo-server-ateramoto.herokuapp.com")) { _ in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("CommonResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }

        let expectation = self.expectation(description: "Create Todo Request")

        APIClient().call(
            request: TodoCreateRequest(body: body),
            success: { response in
                todoCreateResponse = response
                expectation.fulfill()
        }, failure: { _ in
                return
        })

        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todoCreateResponse)
            XCTAssertEqual(todoCreateResponse?.errorCode, 0)
            XCTAssertEqual(todoCreateResponse?.errorMessage, "")
        }
    }
}
