import Foundation

protocol BaseResponse: Codable {
    var errorCode: Int { get }
    var errorMessage: String { get }
}
