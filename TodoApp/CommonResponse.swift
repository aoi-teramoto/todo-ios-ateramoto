import Foundation

struct CommonResponse: BaseResponse {
    var errorCode: Int
    var errorMessage: String
}
