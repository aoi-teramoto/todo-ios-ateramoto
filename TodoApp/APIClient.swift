import Alamofire

final class APIClient {
    private let internalServerErrorMessage = "サーバーで不明なエラーが発生しました"
    private var customDecoder: JSONDecoder {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let dateFormatForServer = DateFormatter()
        dateFormatForServer.timeZone = TimeZone(identifier: "UTC")
        dateFormatForServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        decoder.dateDecodingStrategy = .formatted(dateFormatForServer)
        return decoder
    }
    
    func call<T: RequestProtocol>(request: T, success: @escaping (T.Response) -> Void, failure: @escaping (String) -> Void) {
        let requestUrl = request.baseUrl + request.path
        
        AF.request(requestUrl,
                   method: request.method,
                   parameters: request.parameters,
                   encoding: request.encoding,
                   headers: request.headers
        )
            .validate(statusCode: 200..<300) // 200番台のみ正常系として扱う
            .responseData { response in
                guard let data = response.data else {
                    failure(self.internalServerErrorMessage)
                    return
                }
                switch response.result {
                case .success:
                    do {
                        let result = try self.customDecoder.decode(T.Response.self, from: data)
                        success(result)
                    } catch {
                        failure(self.internalServerErrorMessage)
                    }
                case .failure:
                    let result = try? self.customDecoder.decode(CommonResponse.self, from: data)
                    let message = result?.errorMessage ?? self.internalServerErrorMessage
                    failure(message)
                }
        }
    }
}
