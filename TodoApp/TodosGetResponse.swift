import Foundation

struct TodosGetResponse: BaseResponse {
    var todos: [Todo]
    var errorCode: Int
    var errorMessage: String
}
