import UIKit

protocol TodoEditViewControllerDelegate: class {
    func todoEditViewControllerDidSendTodo(_ todoEditViewController: TodoEditViewController, message: String)
}

final class TodoEditViewController: UIViewController {
    weak var delegate: TodoEditViewControllerDelegate?

    @IBOutlet private weak var detailTextView: UITextView!
    @IBOutlet private weak var registrationButton: UIButton!
    @IBOutlet private weak var titleTextField: UITextField!
    @IBOutlet private weak var dateTextField: UITextField!
    @IBOutlet private weak var countTitleLabel: UILabel!
    @IBOutlet private weak var countDetailLabel: UILabel!

    var todo: Todo?
    private let titleMaxLength = 100
    private let detailMaxLength = 1000
    private let datePicker = UIDatePicker()
    private let formatter = DateFormatter()
    private var titleCount: Int {
        titleTextField.text?.count ?? 0
    }
    private var detailCount: Int {
        detailTextView.text.count
    }
    private var isTitleLimitOver: Bool {
        titleCount > titleMaxLength
    }
    private var isDetailLimitOver: Bool {
        detailCount > detailMaxLength
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailTextView.delegate = self
        
        setUpNavigationItem()
        setUpDetailTextView()
        setUpRegistrationButton()
        setUpDatePicker()
        formatter.dateFormat = "yyyy/M/d"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        if let todo = todo {
            setUpTodoText(todo: todo)
            setUpCountText(todo: todo)
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    @IBAction private func onTappedRegistrationButton(_ sender: Any) {
        guard let title = titleTextField.text else { return }
        let detail = detailTextView.text.isEmpty ? nil : detailTextView.text
        let dateText = dateTextField.text ?? ""
        let date = formatter.date(from: dateText)
        if let id = todo?.id {
            let todo = Todo(id: id, title: title, detail: detail, date: date)
            updateTodo(todoUpdateBody: TodoUpdateRequest(todo: todo))
        } else {
            let body = TodoRequestBody(title: title, detail: detail, date: date)
            createTodo(todoCreateBody: TodoCreateRequest(body: body))
        }
    }
    
    @IBAction private func onChangeTitleLengthCounter(_ sender: Any) {
        countTitleLabel.text = String(titleCount)
        countTitleLabel.textColor = isTitleLimitOver ? .red : .black
        updateRegistrationButtonState()
    }
    
    private func createTodo(todoCreateBody: TodoCreateRequest) {
        APIClient().call(
            request: todoCreateBody,
            success: { [weak self] _ in
                guard let self = self else { return }
                self.delegate?.todoEditViewControllerDidSendTodo(self, message: "登録しました")
                self.navigationController?.popViewController(animated: true)
            },
            failure: { [weak self] message in
                self?.showErrorAlert(message: message)
            }
        )
    }

    private func updateTodo(todoUpdateBody: TodoUpdateRequest) {
        APIClient().call(
            request: todoUpdateBody,
            success: { [weak self] _ in
                guard let self = self else { return }
                self.delegate?.todoEditViewControllerDidSendTodo(self, message: "更新しました")
                self.navigationController?.popViewController(animated: true)
            },
            failure: { [weak self] message in
                self?.showErrorAlert(message: message)
            }
        )
    }

    private func setUpTodoText(todo: Todo) {
        titleTextField.text = todo.title
        detailTextView?.text = todo.detail
        if let date = todo.date {
            dateTextField.text = formatter.string(from: date)
        }
    }

    private func setUpCountText(todo: Todo) {
        countTitleLabel.text = String(titleCount)
        countDetailLabel.text = String(detailCount)
        setUpRegistrationButton()
    }
    
    private func setUpDatePicker() {
        datePicker.datePickerMode = .date
        datePicker.timeZone = NSTimeZone.local
        datePicker.locale = .current
        dateTextField.inputView = datePicker

        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 45))
        let spaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneItem = UIBarButtonItem(title: "決定", style: .done, target: self, action: #selector(onTappedDoneButton))
        let closeItem = UIBarButtonItem(title: "閉じる", style: .plain, target: self, action: #selector(onTappedCloseButton))
        let deleteItem = UIBarButtonItem(title: "削除", style: .plain, target: self, action: #selector(onTappedDeleteButton))
        toolbar.setItems([deleteItem, spaceItem, doneItem, closeItem], animated: true)
        
        dateTextField.inputAccessoryView = toolbar
    }
    
    private func setUpNavigationItem() {
        navigationItem.title = "TODO APP"
    }
    
    private func setUpDetailTextView() {
        detailTextView.layer.borderColor = UIColor.gray.cgColor
        detailTextView.layer.borderWidth = 0.5
        detailTextView.layer.cornerRadius = 5.0
        detailTextView.layer.masksToBounds = true
    }
    
    private func setUpRegistrationButton() {
        registrationButton.setTitleColor(.white, for: .normal)
        registrationButton.backgroundColor = .gray
        registrationButton.layer.borderWidth = 0.5
        registrationButton.layer.borderColor = UIColor.black.cgColor
        registrationButton.layer.cornerRadius = 10.0
        setUpRegistrationButtonTitle()
        updateRegistrationButtonState()
    }

    private func setUpRegistrationButtonTitle() {
        let message = todo?.id == nil ? "登録" : "更新"
        registrationButton.setTitle(message, for: .normal)
    }

    private func updateRegistrationButtonState() {
        if titleCount == 0 || isTitleLimitOver || isDetailLimitOver {
            registrationButton.isEnabled = false
            registrationButton.backgroundColor = .gray
        } else {
            registrationButton.isEnabled = true
            registrationButton.backgroundColor = .blue
        }
    }

    @objc private func onTappedDoneButton() {
        dateTextField.endEditing(true)
        dateTextField.text = formatter.string(from: datePicker.date)
    }
    
    @objc private func onTappedCloseButton() {
        dateTextField.endEditing(true)
    }
    
    @objc private func onTappedDeleteButton() {
        dateTextField.endEditing(true)
        dateTextField.text = nil
    }
}

extension TodoEditViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        countDetailLabel.text = String(detailCount)
        countDetailLabel.textColor = isDetailLimitOver ? .red : .black
        updateRegistrationButtonState()
    }
}
