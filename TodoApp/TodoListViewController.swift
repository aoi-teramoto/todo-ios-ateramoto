import MaterialComponents.MaterialSnackbar
import UIKit

final class TodoListViewController: UIViewController {
    private var todos: [Todo] = []
    private var isDeleteMode = false
    
    @IBOutlet private weak var registrationButton: UIBarButtonItem!
    @IBOutlet private weak var todoTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        todoTableView.dataSource = self
        todoTableView.delegate = self

        setUpNavigationBar()
        setUpNavigationItem()
    }

    // edit画面の戻るボタンタップ時、TODO一覧の取得を行う処理
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchTodoList()
    }
    
    private func fetchTodoList() {
        APIClient().call(
            request: TodosGetRequest(),
            success: { [weak self] result in
                self?.todos = result.todos
                self?.todoTableView.reloadData()
            },
            failure: { [weak self] message in
                self?.showErrorAlert(message: message)
            }
        )
    }

    private func deleteTodo(id: Int) {
        APIClient().call(
            request: TodoDeleteRequest(id: id),
            success: { [weak self] _ in
                self?.fetchTodoList()
            },
            failure: { [weak self] message in
                self?.showErrorAlert(message: message)
            }
        )
    }
    
    @IBAction private func onTappedDeleteButton(_ sender: UIBarButtonItem) {
        isDeleteMode.toggle()
        let systemItem: UIBarButtonItem.SystemItem = isDeleteMode ? .reply : .trash
        let deleteModeButton = UIBarButtonItem(barButtonSystemItem: systemItem, target: self, action: #selector(onTappedDeleteButton(_:)))
        navigationItem.rightBarButtonItems = [deleteModeButton, registrationButton]
    }
    
    @IBAction private func onTappedRegistrationButton(_ sender: UIBarButtonItem) {
        transitionToEditScreen()
    }

    private func transitionToEditScreen(todo: Todo? = nil) {
        let storyboard = UIStoryboard(name: "Edit", bundle: nil)
        guard let todoEditViewController = storyboard.instantiateViewController(withIdentifier: "Edit") as? TodoEditViewController else { return }
        todoEditViewController.delegate = self
        todoEditViewController.todo = todo
        navigationController?.pushViewController(todoEditViewController, animated: true)
    }

    private func setUpNavigationItem() {
        navigationItem.title = "TODO APP"
    }
    
    private func setUpNavigationBar() {
        navigationController?.navigationBar.barTintColor = .blue
        navigationController?.navigationBar.tintColor = .white
        navigationItem.backBarButtonItem = UIBarButtonItem()
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
    }
    
    private func showSnackbar(text: String) {
        let message = MDCSnackbarMessage()
        message.text = text
        MDCSnackbarManager.show(message)
    }

    private func showDeleteConfirmationAlert(todo: Todo) {
        let alertController = UIAlertController(title: nil, message: "「\(todo.title)」を削除します。\nよろしいですか？", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default) { _ in
            self.deleteTodo(id: todo.id)
        })
        alertController.addAction(UIAlertAction(title: "キャンセル", style: .cancel))
        present(alertController, animated: true)
    }
}

extension TodoListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TodoCell", for: indexPath)
        cell.textLabel?.text = todos[indexPath.row].title
        return cell
    }
}

extension TodoListViewController: TodoEditViewControllerDelegate {
    func todoEditViewControllerDidSendTodo(_ todoEditViewController: TodoEditViewController, message: String) {
        showSnackbar(text: message)
    }
}

extension TodoListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let todo = todos[indexPath.row]
        if isDeleteMode {
            showDeleteConfirmationAlert(todo: todo)
        } else {
            transitionToEditScreen(todo: todo)
        }
    }
}
