import Foundation

struct TodoRequestBody {
    let title: String
    let detail: String?
    let date: Date?
}
