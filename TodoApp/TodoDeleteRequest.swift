import Alamofire

struct TodoDeleteRequest: RequestProtocol {
    typealias Response = CommonResponse
    
    let id: Int
    
    var parameters: Parameters? {
        return nil
    }
    var path: String {
        return "/todos/\(id)"
    }
    var method: HTTPMethod {
        return .delete
    }
}
