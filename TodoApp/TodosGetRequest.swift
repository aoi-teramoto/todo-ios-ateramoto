import Alamofire

struct TodosGetRequest: RequestProtocol {
    typealias Response = TodosGetResponse
    var parameters: Parameters? {
        return nil
    }
    var path: String {
        return "/todos"
    }
    var method: HTTPMethod {
        return .get
    }
}
