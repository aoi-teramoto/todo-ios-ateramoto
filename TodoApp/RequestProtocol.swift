import Alamofire

protocol RequestProtocol {
    associatedtype Response: Codable
    var baseUrl: String { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var encoding: ParameterEncoding { get }
    var parameters: Parameters? { get }
    var headers: HTTPHeaders? { get }
}

extension RequestProtocol {
    var baseUrl: String {
        "https://todo-server-ateramoto.herokuapp.com/"
    }
    var encoding: ParameterEncoding {
        JSONEncoding.default
    }
    var headers: HTTPHeaders? {
        ["Content-Type": "application/json"]
    }
}
